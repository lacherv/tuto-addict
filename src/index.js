import React from 'react';
import ReactDom from 'react-dom';

// CSS
import './index.css';

import { data } from './books';
import Livre from './Book';
import {greeting} from './testing/testing';

function BookList() {
  console.log(greeting);
  return (
    <section className="booklist">
      {data.map((book, index) => {
        return (
          <Livre key={book.id} {...book}></Livre>
        );
      })}
    </section>
  );
}

ReactDom.render(<BookList />, document.getElementById('root'));