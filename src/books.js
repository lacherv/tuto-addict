import f_image from './book.jpg';
import s_image from './book_1.jpg';
import t_image from './book_2.jpg';

export const data = [{
        id: 1,
        img: f_image,
        title: 'How Successful People Think : Change Your Thinking, Change Your Life.',
        author: 'John C. Maxwell',
    },
    {
        id: 2,
        img: s_image,
        title: 'Things Fall Apart',
        author: 'Chinua Achebe',
    },
    {
        id: 3,
        img: t_image,
        title: 'The Art of Reading Minds - Understand Others to Get What You Want',
        author: 'Henrik Fexeus',
    },
];