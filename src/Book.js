import React from 'react';

const Book = ({ img, title, author }) => {
  // attribute, eventHnadler
  // onClick, on MouseOver
  const clickHandler = (e) => {
    console.log(e);
    console.log(e.target);
    alert('hello World');
  };

  const complexExample = (author) => {
    console.log(author);
  };

  return (
    <article className='book' onMouseOver={() => {
      console.log(title);
    }}>
      <img src={img} alt="" />
      <p onClick={() => { console.log(author) }}>{title}</p>
      <h4>{author}</h4>
      <button type="button" onClick={clickHandler}>reference example</button>
      <button type="button" onClick={() => complexExample(author)}>More complex example</button>
    </article>
  )
};

export default Book;
